let pendingList = document.getElementById("pending");
let completedList = document.getElementById("completed");
var arr=[]
function newElement() {
	var inputValue = document.getElementById("myInput").value;
	if (inputValue === '') {
		alert("You must write something!");
	} else {
        arr.push({value:inputValue,done:false})
		console.log(arr)
	}
	document.getElementById("myInput").value = "";
	showList();
}
function removeTask(index){
    arr.splice(index,1);
    showList();
}
function moveToPending(e){
    e.done=false;
    showList();
}
function moveToDone(e){
    e.done=true;
    showList();
}

function showList(){
    pendingList.innerHTML="";
	completedList.innerHTML="";
    for(let i=0;i<arr.length;i++){
        let e = arr[i];
        var li = document.createElement("li");
		var t = document.createTextNode(e.value);
		li.appendChild(t);
		var span = document.createElement("SPAN");
		span.className = "close";
		let deleteButton = document.createElement("button");
		deleteButton.className="delete";
        deleteButton.innerText="Delete";
        deleteButton.onclick = ()=>{
            removeTask(i);
        }
        span.appendChild(deleteButton)
		li.appendChild(span);
		if(e.done){
            let undoButton = document.createElement("button");
            undoButton.innerText="Add To Pending";
			undoButton.className="undo"
            undoButton.onclick=()=>{
                moveToPending(e);
            }
			span.appendChild(undoButton)
		    li.appendChild(span);
            completedList.appendChild(li);
        }
        else{
            let doneButton = document.createElement("button");
			doneButton.className="do"
            doneButton.innerText="Add To Completed"
            doneButton.onclick=()=>{
                moveToDone(e);
            }
            span.appendChild(doneButton);
		    li.appendChild(span);
			pendingList.appendChild(li);
        }
    }
}
